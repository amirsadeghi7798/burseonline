# Generated by Django 3.1.5 on 2021-02-21 09:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eshop_products', '0006_auto_20210221_1110'),
    ]

    operations = [
        migrations.AddField(
            model_name='videos',
            name='description',
            field=models.TextField(null=True, verbose_name='توضیحات'),
        ),
    ]
